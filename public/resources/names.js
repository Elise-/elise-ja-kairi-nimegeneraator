function getNames() {
  fetch('http://localhost:8080/admin/names')
    //{
    //     headers: addAuthorizationHeader({})
    //   })
    // .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => response.json())
    .then(names => displayNames(names));
}

function validateName() {
  const name = document.getElementById('nameName').value;

  if (name == null || name.length < 1) {
    displayNameValidationError("Sugu ei ole valitud!");
    return false;
  }
  // hideNameValidationError();
  return true;
}

var boyNameSelected = false;

function selectBoyName() {
  let boyButton = document.getElementById('boyNameButton');
  if (boyNameSelected === true) {
    boyNameSelected = false;
    boyButton.classList.remove("btn-primary");
    boyButton.classList.add("btn-outline-primary");
  } else {
    boyNameSelected = true;
    boyButton.classList.remove("btn-outline-primary");
    boyButton.classList.add("btn-primary");
    if (unisexNameSelected) {
      selectUnisexName();
    }
    if (girlNameSelected) {
      selectGirlName();
    }
  }
}

var unisexNameSelected = false;

function selectUnisexName() {
  let unisexButton = document.getElementById('unisexNameButton');
  if (unisexNameSelected === true) {
    unisexNameSelected = false;
    unisexButton.classList.remove("btn-success");
    unisexButton.classList.add("btn-outline-success");
  } else {
    unisexNameSelected = true;
    unisexButton.classList.remove("btn-outline-success");
    unisexButton.classList.add("btn-success");
    if (boyNameSelected) {
      selectBoyName();
    }
    if (girlNameSelected) {
      selectGirlName();
    }
  }
}

var girlNameSelected = false;

function selectGirlName() {
  let girlButton = document.getElementById('girlNameButton');
  if (girlNameSelected === true) {
    girlNameSelected = false;
    girlButton.classList.remove("btn-danger");
    girlButton.classList.add("btn-outline-danger");
  } else {
    girlNameSelected = true;
    girlButton.classList.remove("btn-outline-danger");
    girlButton.classList.add("btn-danger");
    if (boyNameSelected) {
      selectBoyName();
    }
    if (unisexNameSelected) {
      selectUnisexName();
    }
  }
}

var internationalNameSelected = false;

function selectInternationalName() {
  let internationalButton = document.getElementById('internationalNameButton');
  if (internationalNameSelected === true) {
    internationalNameSelected = false;
    internationalButton.classList.remove("btn-warning");
    internationalButton.classList.add("btn-outline-warning");
  } else {
    internationalNameSelected = true;
    internationalButton.classList.remove("btn-outline-warning");
    internationalButton.classList.add("btn-warning");
  }
}

var nameLengthSelection = 0;

function selectNameLength() {
  nameLengthSelection = document.getElementById('nameLengthSelect').value;
}

var nameFirstLetter = "";

function selectFirstLetter() {
  nameFirstLetter = document.getElementById('nameFirstLetter').value;
}

var nameOneLetter = "";

function selectOneLetter() {
  nameOneLetter = document.getElementById('nameOneLetter').value;
}

var nameLastLetter = "";

function selectLastLetter() {
  nameLastLetter = document.getElementById('nameLastLetter').value;
}

function searchNames() {
  let searchFilter = {
    boySelected: boyNameSelected,
    unisexSelected: unisexNameSelected,
    girlSelected: girlNameSelected,
    internationalSelected: internationalNameSelected,
    lengthSelection: nameLengthSelection,
    firstLetter: nameFirstLetter,
    oneLetter: nameOneLetter,
    lastLetter: nameLastLetter
  };

  fetch('http://localhost:8080/names/search', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(searchFilter)
    })
    .then(response => response.json())
    .then(names => displayNames(names));

}

function getAdminNames() {
  if (isAuthenticated()) {
    fetch('http://localhost:8080/admin/names')
      .then(response => response.json())
      .then(names => displayNames(names));
  } else {
    document.location = "/login.html";
  }
}

function addName() {
  const name = document.getElementById('nameName').value;
  const sex = document.getElementById('nameSex').value;
  const international = document.getElementById('nameInternational').value;

  const addUrl = 'http://localhost:8080/admin/name'
  fetch(addUrl, {
      method: 'POST',
      headers: addAuthorizationHeader({
        "Content-Type": "application/json"
      }),
      body: JSON.stringify({
        'name': name,
        'sex': sex,
        'international': international
      })
    })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => {
      getNames();
      closeNameModal();
    });
}

function deleteName(id) {
  if (confirm('Kas sa oled kindel, et soovid nime kustutada?')) {
    const deleteUrl = "http://localhost:8080/admin/name/" + id;
    fetch(deleteUrl, {
        method: 'DELETE',
        headers: addAuthorizationHeader({
          "Content-Type": "application/json"
        })
      })
      .then(response => clearTokenAndRedirectIfUnauthorized(response))
      .then(response => getAdminNames());
  }
}

function openNameModal(id) {

  document.getElementById('nameId').value = null;
  document.getElementById('nameName').value = null;
  document.getElementById('nameSex').value = null;
  document.getElementById('nameInternational').value = null;
  $('#exampleModal').modal('show');
  if (id > 0) {
    fetch('http://localhost:8080/names/' + id, {
        headers: addAuthorizationHeader({})
      })
      .then(response => clearTokenAndRedirectIfUnauthorized(response))
      .then(response => response.json())
      .then(name => {
        document.getElementById('nameId').value = name.id;
        document.getElementById('nameName').value = name.name;
        document.getElementById('nameSex').value = name.sex;
        document.getElementById('nameInternational').value = name.international;
      });
  }
}

function closeNameModal() {
  $('#exampleModal').modal('hide');
}

function addOrEditName() {
  if (validateName()) {
    const id = document.getElementById('nameId').value;
    if (id > 0) {
      editName();
    } else {
      addName();
    }
  }
}